package com.company;

public abstract class GameObject {
    private String name;
    private String rank;

    public GameObject(String name){
        this.name = name;
    }
    public String getRank() {
        return rank;
    }
}