package sample.utils;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectorDB {
    public Connection databaseLink;

    public Connection getConnection(){
        String databaseName = "guild";
        String databaseUser = "root";
        String databasePassword = "";
        String url = "jdbc:mysql://localhost/"+databaseName;

        try {
            Class.forName("com.mysql.jdbc.Driver");
            databaseLink = DriverManager.getConnection(url, databaseUser, databasePassword);

        }catch (Exception e){
            e.printStackTrace();
            e.getCause();
        }
        return  databaseLink;
    }
}
