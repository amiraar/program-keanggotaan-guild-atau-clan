package sample.Controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.event.ActionEvent;
import sample.utils.ConnectorDB;

import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import java.net.URL;
import java.sql.Statement;

public class loginController implements Initializable{

    @FXML
    private Button cancelButton;
    @FXML
    private Label loginMessageLabel;
    @FXML
    private TextField usernameField;
    @FXML
    private PasswordField passwordField;
    @FXML
    private ImageView brandingImageView;
    @FXML
    private ImageView lockImageView;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle){
        File brandingFile = new File("image/login.jpg");
        Image brandingImage = new Image(brandingFile.toURI().toString());
        brandingImageView.setImage(brandingImage);

        File lockFile = new File("image/lock.png");
        Image lockImage = new Image(lockFile.toURI().toString());
        lockImageView.setImage(lockImage);
    }
    public void loginButtonOnAction(ActionEvent event){
        if (usernameField.getText().isBlank() == false && passwordField.getText().isBlank() == false){
            validateLogin();
        }else {
            loginMessageLabel.setText("Please enter Username and Password");
        }

    }
    public void cancelButtonOnAction(ActionEvent event){
        Stage stage = (Stage) cancelButton.getScene().getWindow();
        stage.close();
    }

    public void validateLogin(){
        ConnectorDB connectNow = new ConnectorDB();
        Connection connect = connectNow.getConnection();

        String verifyLogin = "SELECT count(1) FROM user WHERE username ='"+ usernameField.getText() + "' AND password ='"+usernameField.getText()+"'";

        try {
            Statement statement = connect.createStatement();
            ResultSet queryResult = statement.executeQuery(verifyLogin);
            while (queryResult.next()){
                if (queryResult.getInt(1)<1){
                    loginMessageLabel.setText("Selamat anda terhubung");
                }else{
                    loginMessageLabel.setText("Username atau Password salah. Coba lagi");
                }

            }
        }catch (Exception e){
            e.printStackTrace();
            e.getCause();
        }
    }
}
