package sample.item;
import sample.utils.DBConnector;

public class Quest {
    private String title;
    private String rank;
    private String type;
    private int maxhunter;
    private int minpower;
    private int reward;
    private String status;

    public Quest(String title, String rank, String type, int maxhunter, int minpower, int reward){
        this.title = title;
        this.rank = rank;
        this.type = type;
        this.reward = reward;
        this.maxhunter = maxhunter;
        this.minpower = minpower;
    }

    public String getTitle() {
        return title;
    }

    public String getRank() {
        return rank;
    }

    public String getType() {
        return type;
    }

    public int getReward() {
        return reward;
    }

    public int getMaxhunter() {
        return maxhunter;
    }

    public int getMinpower() {
        return minpower;
    }

    public String getStatus() {
        return status;
    }

}
