package sample.item;

public abstract class gameObject {
    private String name;
    private String rank;

    public gameObject(String name){
        this.name = name;
    }
    public abstract String getRank();
}
