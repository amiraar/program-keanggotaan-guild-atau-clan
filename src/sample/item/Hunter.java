package sample.item;

import java.util.ArrayList;
import java.util.Scanner;

public class Hunter extends User{
    private String rank;
    private int power;
    private int money;
    ArrayList<Quest> quests;
    private String party;

    public Hunter(){}
    public Hunter(String username, String password){
        quests = new ArrayList<>();
        setUsername(username);
        setPassword(password);
    }
    public Hunter(String name, String gender, int age, String password, String username) {
        super(name, gender, age, password, username);
    }

    public void readQuest(){

    }

    public void takeQuest(){

    }
    public void submitQuest(){

    }
}
