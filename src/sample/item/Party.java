package sample.item;

import java.util.ArrayList;

public class Party {
    private String name;
    private String rank;
    ArrayList<Hunter> anggota;
    private int power;
    ArrayList<Quest> quests;

    public Party(String name, String rank){
        this.name = name;
        this.rank = rank;
        anggota = new ArrayList<>();
    }
}
