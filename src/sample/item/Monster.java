package sample.item;

public class Monster extends gameObject {
    private int power;

    public Monster(String nama, int power){
        super(nama);
        this.power = power;
    }

    @Override
    public String getRank(){
        if(power > 300){
            return "Rank S";
        }else if(power <= 300 || power > 200){
            return "Rank A";
        }else if(power <= 200 || power > 80){
            return "Rank B";
        }else if(power <= 80 || power > 30){
            return "Rank C";
        }else{
            return "underrated";
        }
    }
}
