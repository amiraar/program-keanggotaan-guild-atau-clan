package sample.item;

import java.util.ArrayList;

public class Guild {
    private String nama;
    ArrayList<Hunter> members;
    ArrayList<Quest> quests;
    ArrayList<Party> parties;

    public Guild(String nama){
        this.nama = nama;
        members = new ArrayList<>();
        quests = new ArrayList<>();
        parties = new ArrayList<>();
    }

    public ArrayList<Hunter> getMembers(){
        return members;
    }
    public ArrayList<Quest> getQuests(){
        return quests;
    }
    public ArrayList<Party> getParties(){
        return parties;
    }

    public void addMember(Hunter hunter){
        members.add(hunter);
    }
    public void removeMember(Hunter hunter){
        members.remove(hunter);
    }

    public void addQuests(Quest quest){
        quests.add(quest);
    }
    public void removeQuests(Quest quest){
        quests.remove(quest);
    }

    public void addPartys(Party party){
        parties.add(party);
    }
    public void removePartys(Party party){
        parties.remove(party);
    }
}
