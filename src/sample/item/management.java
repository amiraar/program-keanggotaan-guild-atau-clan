package sample.item;

import java.util.ArrayList;

public interface management <T>{
    enum MODE {Hunter, Quest}

    void create(T object, Guild guild, MODE mode);

    ArrayList<T> read(Guild guild, MODE mode);

    void update(int index, T object, Guild guild, MODE mode);

    void delete();
}
