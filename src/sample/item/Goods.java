package sample.item;

public class Goods extends gameObject {
    private int price;

    public Goods(String nama, int price){
        super(nama);
        this.price = price;
    }

    @Override
    public String getRank() {
        if(price > 100){
            return "Rank S";
        }else if(price <= 100 || price > 50){
            return "Rank A";
        }else if(price <= 50 || price > 10){
            return "Rank B";
        }else{
            return "Rank C";
        }
    }
}
